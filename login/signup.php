<?php
$db= new PDO('mysql:host=localhost;dbname=login','root','');
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign Up</title>
</head>
<body>

<form action="signup.php" method="post" enctype="multipart/form-data">
    <label for="username">Username :</label> <input id="username" type="text" name="username"><br>
    <label for="email">Email : </label> <input id="email" type="email" name="email"><br>
    <label for="password">Password : </label><input id="password" type="password" name="password"><br>
    <input type="submit" name="signup" value="Sign Up">
</form>
<br>
Already have an account please <a href="index.php">Login</a>
<br>
</body>
</html>

<?php


if(isset($_POST['signup']))
{
if(empty($_POST['username'])||empty($_POST['email']) || empty($_POST['password']))
{
echo "Fields can't be empty";

}
else
{
$username=$_POST['username'];
$username=strip_tags($username);
$username = filter_var($username, FILTER_SANITIZE_STRING);
$username=htmlspecialchars($username,ENT_QUOTES, 'UTF-8');

$email=filter_var($_POST['email'],FILTER_VALIDATE_EMAIL);
$email=filter_var($email,FILTER_SANITIZE_EMAIL);

$options = [
'cost' => 11
];

$password=password_hash($_POST['password'],PASSWORD_DEFAULT,$options);

$stmt=$db->prepare("INSERT INTO user (username, email, password) VALUES (:username,:email,:password)");
$stmt->bindParam(':username',$username);
$stmt->bindParam(':email',$email);
$stmt->bindParam(':password',$password);
$ok=$stmt->execute();

if($ok)
{
echo 'Success';
}

}

}


