<?php
$db= new PDO('mysql:host=localhost;dbname=login','root','');
/*
if (!isset($_GET['page']))
{
    include_once 'signin.php';
}

if (isset($_GET['page']))
{
    switch ($_GET['page'])
    {
        case "signup":
            include_once 'signup.php';
            break;
        case "signin":
            include_once 'signin.php';
            break;

    }

}
*/
include_once 'signin.php';
?>
<br>
    <a href="signup.php">Create New Account</a>
<br>
<?php



if(isset($_POST['login']))
{
    $email=$_POST['email'];
    $password=$_POST['password'];

    if(empty($email) || empty($password))
    {
        echo "Fields can't be empty";
    }
    else
    {
        $stmt= $db->prepare('SELECT email,password FROM user WHERE email=:email');
        $stmt->bindParam(":email",$email);
        $stmt->execute();
        $ok=$stmt->rowCount();
        if(!$ok)
        {
            echo 'Your email is incorrect.<br>';
        }
        $value=$stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($value as $row)
        {
            if (password_verify($password, $row['password'])) {
                header('Location: profile.php');
            } else {
                echo 'Your password is incorrect.<br>';
            }
        }
    }


}